CREATE PROCEDURE ENDO.GetDiaflexToDo( @StudyId INT ) AS
BEGIN
  SELECT v.*, p.NationalId, CONCAT('Utfylt: ', dbo.ShortTime( eprom.EventTime ) ) AS InfoText
  FROM dbo.ViewActiveCaseListStub v
  JOIN dbo.Person p ON p.PersonId = v.PersonId
  JOIN dbo.GetLastFormTableByName( 'DIAFLEX_ePROM', NULL ) eprom ON eprom.PersonId = v.PersonId
  LEFT JOIN dbo.GetLastEnumValues( 12742,NULL ) ev ON ev.PersonId = v.PersonId AND ev.EventTime = eprom.EventTime
  WHERE v.StudyId = @StudyId AND ISNULL( ev.EnumVal, -1 ) = -1;
END
 