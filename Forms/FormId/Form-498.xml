<?xml version="1.0"?>
<StudyForm>
  <Form FormId="498" FormName="NDVPHT" FormTitle="Startskjema">
    <Page PageTitle="Grunndata">
      <Item ItemId="3389" ItemType="2" VarName="NDV_CONSENT" LoincScale="ORD">
        <FormItem ItemText="Gitt samtykke til registeret" ItemHelp="Norsk diabetesregister for voksne. Kontaktperson Karianne Fjeld Løvaas." Visibility="1" Highlight="1" Optional="0" ClearStrategy="3">
          <Answer AnswerId="16869" OrderNumber="1" OptionText="Ja" VerboseText="Samtykke gitt til diabetesregisteret." ShortCode="J"/>
          <Answer AnswerId="16870" OrderNumber="2" OptionText="Nei" VerboseText="Ikke samtykke til diabetesregisteret." ShortCode="N"/>
          <Answer AnswerId="19223" OrderNumber="3" OptionText="Trukket tilbake" VerboseText="Samtykke til diabetesregisteret trukket tilbake." ShortCode="Tr"/>
          <Answer AnswerId="20850" OrderNumber="4" OptionText="Ukjent" ShortCode="U"/>
        </FormItem>
      </Item>
      <Item ItemId="3196" ItemType="2" VarName="NDV_TYPE" LoincScale="ORD">
        <FormItem ItemText="Type diabetes" Visibility="1" Highlight="1" Optional="0">
          <Answer AnswerId="15986" OrderNumber="1" OptionText="Type 1 (inkl. LADA)" VerboseText="Type 1 diabetes." ShortCode="1" ICD10="E10"/>
          <Answer AnswerId="15987" OrderNumber="2" OptionText="Type 2" VerboseText="Type 2 diabetes." ShortCode="2" ICD10="E11"/>
          <Answer AnswerId="15988" OrderNumber="3" OptionText="Annen diabetestype (inkl. pankreatitt, MODY)" VerboseText="Annen diabetestype." ShortCode="A"/>
          <Answer AnswerId="16137" OrderNumber="4" OptionText="er ikke avklart." ShortCode="U" ICD10="E14"/>
          <Answer AnswerId="40942" OrderNumber="5" OptionText="Svangerskapsdiabetes." ShortCode="S" ICD10="O24"/>
        </FormItem>
      </Item>
      <Item ItemId="3486" ItemType="1" VarName="NDV_DIAGNOSE_YYYY" LoincScale="QN">
        <FormItem ItemHeader="Debut" ItemText="Diagnosen stilt, årstall" ItemHelp="Angi årstall.  Svangerskapsdiabetes unntas." MinExpression="DOB_YYYY" MaxExpression="NOW_YYYY" Visibility="1" Highlight="1" Optional="0">
          <Answer/>
        </FormItem>
      </Item>
    </Page>
    <Page PageTitle="Funn og resultater">
      <Item ItemId="3225" ItemType="1" VarName="HEIGHT" LoincScale="QN" UnitStr="cm">
        <FormItem ItemText="Høyde" ItemHelp="Uten sko." MinExpression="50" MaxExpression="220" Visibility="1" Highlight="1" Optional="0">
          <Answer/>
        </FormItem>
      </Item>
      <Item ItemId="3224" ItemType="1" VarName="WEIGHT" LoincScale="QN" UnitStr="kg">
        <FormItem ItemText="Vekt" ItemHelp="Uten sko og yttertøy." MinExpression="10" MaxExpression="400" Visibility="1" Highlight="1" Optional="0" Decimals="1">
          <Answer/>
        </FormItem>
      </Item>
      <Item ItemId="3310" ItemType="1" VarName="BMI" LoincScale="QN" UnitStr="kg/m2">
        <FormItem ItemText="KMI (kg/m2)" ItemHelp="beregnet fra høyde og vekt" Expression="WEIGHT/((HEIGHT/100)*(HEIGHT/100)) * ISPOS(WEIGHT)*ISPOS(HEIGHT)" MinExpression="5" MaxExpression="150" Visibility="1" Highlight="1" Optional="1" Decimals="1">
          <Answer/>
        </FormItem>
      </Item>
      <Item ItemId="3230" ItemType="1" VarName="SYSBP" LoincScale="QN" UnitStr="mmHg">
        <FormItem ItemText="Systolisk blodtrykk" ItemHelp="Standard BT-måling, dvs. gj.snittet av de to siste av tre målinger med 1 min. mellom." MinExpression="50" MaxExpression="250" Visibility="1" Highlight="1" Optional="0" FormatStr="Blodtrykk %s">
          <Answer/>
        </FormItem>
      </Item>
      <Item ItemId="3231" ItemType="1" VarName="DIABP" LoincScale="QN" UnitStr="mmHg">
        <FormItem ItemText="Diastolisk blodtrykk" ItemHelp="Standard BT-måling, dvs. gj.snittet av de to siste av tre målinger med 1 min. mellom." MinExpression="30" MaxExpression="150" Visibility="1" Highlight="1" Optional="0" FormatStr="¬/%s mmHg.">
          <Answer/>
        </FormItem>
      </Item>
      <Item ItemId="3227" ItemType="2" VarName="NDV_SMOKING" LoincScale="ORD">
        <FormItem ItemText="Røykestatus" ItemHelp="Velg Dagligrøyker hvis sluttet for mindre enn 3 mnd siden." Visibility="1" Highlight="1" Optional="0">
          <Answer AnswerId="16138" OrderNumber="1" OptionText="Aldri røykt daglig."/>
          <Answer AnswerId="16139" OrderNumber="2" OptionText="Dagligrøyker."/>
          <Answer AnswerId="16140" OrderNumber="3" OptionText="Eks-dagligrøyker."/>
          <Answer AnswerId="16141" OrderNumber="4" OptionText="Røykestatus ukjent.*" ShortCode="U"/>
        </FormItem>
      </Item>
    </Page>
    <Page PageTitle="Aktuell behandling">
      <Item ItemId="4207" ItemType="2" VarName="NDV_NONPHARMA_ONLY" LoincScale="ORD">
        <FormItem ItemText="Bare kost/mosjon" Visibility="1" Highlight="1" Optional="1">
          <Answer AnswerId="22300" OrderNumber="1" OptionText="Ja"/>
          <Answer AnswerId="22301" OrderNumber="2" OptionText="Nei" ShortCode="N"/>
          <Answer AnswerId="22302" OrderNumber="3" OptionText="Vet ikke" ShortCode="V"/>
        </FormItem>
      </Item>
    </Page>
    <Page PageTitle="">
      <Item ItemId="4754" ItemType="2" VarName="NDV_METFORMIN" LoincScale="ORD">
        <FormItem ItemText="Metformin" ItemHelp="Inkludert sammensatte preparater med metformin" Visibility="1" Highlight="1" Optional="1">
          <Answer AnswerId="26368" OrderNumber="1" OptionText="Ja" VerboseText="Metformin." ShortCode="J"/>
          <Answer AnswerId="26369" OrderNumber="2" OptionText="Nei" ShortCode="N"/>
          <Answer AnswerId="26370" OrderNumber="3" OptionText="Ukjent" ShortCode="U"/>
        </FormItem>
      </Item>
      <Item ItemId="4755" ItemType="2" VarName="NDV_SULFONYLUREA" LoincScale="ORD">
        <FormItem ItemText="Sulfonylurea" ItemHelp="Inkludert sammensatte preparater med sulfonylurea" Visibility="1" Highlight="1" Optional="1">
          <Answer AnswerId="26353" OrderNumber="1" OptionText="Ja" VerboseText="Sulfonylurea." ShortCode="J"/>
          <Answer AnswerId="26354" OrderNumber="2" OptionText="Nei" ShortCode="N"/>
          <Answer AnswerId="26355" OrderNumber="3" OptionText="Ukjent" ShortCode="U"/>
        </FormItem>
      </Item>
      <Item ItemId="4810" ItemType="2" VarName="ATC_A10B_CTOX" LoincScale="ORD">
        <FormItem ItemText="Andre antidiabetika" ItemHelp="Glitazoner, inkretiner, repaglinid, nateglinid, akarbose." Visibility="1" Optional="1">
          <Answer AnswerId="26948" OrderNumber="1" OptionText="Ja" VerboseText="Andre midler." ShortCode="J"/>
          <Answer AnswerId="26949" OrderNumber="2" OptionText="Nei" ShortCode="N"/>
          <Answer AnswerId="26950" OrderNumber="3" OptionText="Ukjent" ShortCode="U"/>
        </FormItem>
      </Item>
      <Item ItemId="4726" ItemType="2" VarName="NDV_INSULINADM" LoincScale="ORD">
        <FormItem ItemText="Insulin" Visibility="1" Highlight="1" Optional="1">
          <Answer AnswerId="25473" OrderNumber="1" OptionText="Nei"/>
          <Answer AnswerId="25474" OrderNumber="2" OptionText="Sprøyte/penn"/>
          <Answer AnswerId="25475" OrderNumber="3" OptionText="Pumpe"/>
          <Answer AnswerId="25476" OrderNumber="4" OptionText="Vet ikke"/>
        </FormItem>
      </Item>
    </Page>
    <Page PageTitle="Annen behandling*">
      <Item ItemId="3336" ItemType="2" VarName="ATC_B01AC" LoincScale="ORD">
        <FormItem ItemHeader="Bruker platehemmer" ItemText="Albyl-E/Andre platehemmere" ItemHelp="Acetylsalicylsyre, Albyl-E&amp;reg;, Asasantin Retard&amp;reg;, Persantin&amp;reg;, Plavix&amp;reg; Ticlid&amp;reg;, evt. andre i ATC gruppe B01AC." Visibility="1" Highlight="1" Optional="0">
          <Answer AnswerId="16545" OrderNumber="1" OptionText="Ja" VerboseText="Bruker platehemmer." ShortCode="J"/>
          <Answer AnswerId="16546" OrderNumber="2" OptionText="Nei" ShortCode="N"/>
          <Answer AnswerId="16547" OrderNumber="3" OptionText="Ukjent" ShortCode="U"/>
        </FormItem>
      </Item>
      <Item ItemId="4003" ItemType="2" VarName="ATC_B01AA" LoincScale="ORD">
        <FormItem ItemText="Marevan" ItemHelp="Marevan&amp;reg;, Warfarin, Pradaxa&amp;reg;, Eliquis&amp;reg;, Xarelto&amp;reg;" Visibility="1" Highlight="1" Optional="1">
          <Answer AnswerId="20865" OrderNumber="1" OptionText="Ja" VerboseText="Antikoagulert." ShortCode="J"/>
          <Answer AnswerId="20866" OrderNumber="2" OptionText="Nei" ShortCode="N"/>
          <Answer AnswerId="20867" OrderNumber="3" OptionText="Ukjent" ShortCode="U"/>
        </FormItem>
      </Item>
      <Item ItemId="3337" ItemType="2" VarName="ATC_C10A" LoincScale="ORD">
        <FormItem ItemText="Lipidsenkende" ItemHelp="Cholestagel&amp;reg;, Crestor&amp;reg;, Ezetrol&amp;reg;, Inegy&amp;reg;, Lescol&amp;reg;, Lestid&amp;reg;, Lipitor&amp;reg;, Lovastatin, Mevacor&amp;reg;, Niaspan&amp;reg;, Omacor&amp;reg;, Pravachol&amp;reg;, Pravastatin, Questran&amp;reg;, Simvastatin, Sortis&amp;reg;, Tredaptive&amp;reg;, Zocor&amp;reg;," Visibility="1" Highlight="1" Optional="0">
          <Answer AnswerId="16548" OrderNumber="1" OptionText="Ja" VerboseText="Bruker lipidsenkende." ShortCode="J"/>
          <Answer AnswerId="16549" OrderNumber="2" OptionText="Nei" ShortCode="N"/>
          <Answer AnswerId="16550" OrderNumber="3" OptionText="Ukjent" ShortCode="U"/>
        </FormItem>
      </Item>
      <Item ItemId="3339" ItemType="2" VarName="ATC_C09" LoincScale="ORD">
        <FormItem ItemText="ACE hemmer/AII blokker" ItemHelp="Aprovel&amp;reg;, Atacand&amp;reg;, Capoten&amp;reg;, Captopril, CoAprovel&amp;reg;, Cozaar&amp;reg;, Diovan&amp;reg;, Enalapril, Gopten&amp;reg;, Lisinopril, Losartan, Micardis&amp;reg;, MicardisPlus&amp;reg;, Olmetec&amp;reg;, Ramipril, Renitec&amp;reg;, Teveten&amp;reg;, Triatec&amp;reg;, Vivatec&amp;reg;, Zestoretic&amp;reg;, Zestril&amp;reg; (pluss alle comp/mite varianter)" Visibility="1" Highlight="1" Optional="0">
          <Answer AnswerId="16566" OrderNumber="1" OptionText="Ja" VerboseText="Bruker ACE/A2-blokker." ShortCode="J"/>
          <Answer AnswerId="16567" OrderNumber="2" OptionText="Nei" ShortCode="N"/>
          <Answer AnswerId="16568" OrderNumber="3" OptionText="Ukjent" ShortCode="U"/>
        </FormItem>
      </Item>
      <Item ItemId="5309" ItemType="2" VarName="PHT_BPDRUGS" LoincScale="ORD">
        <FormItem ItemText="Totalt antall BT medikamenter" Visibility="1" Optional="1">
          <Answer AnswerId="28839" OrderNumber="0" OptionText="0"/>
          <Answer AnswerId="28840" OrderNumber="1" OptionText="1"/>
          <Answer AnswerId="28841" OrderNumber="2" OptionText="2"/>
          <Answer AnswerId="28842" OrderNumber="3" OptionText="3"/>
          <Answer AnswerId="28843" OrderNumber="4" OptionText="4"/>
          <Answer AnswerId="28844" OrderNumber="5" OptionText="5"/>
          <Answer AnswerId="28845" OrderNumber="6" OptionText="Mer enn 5"/>
          <Answer AnswerId="28846" OrderNumber="7" OptionText="Vet ikke"/>
        </FormItem>
      </Item>
    </Page>
    <Page PageTitle="Viktige komplikasjoner">
      <Item ItemId="3397" ItemType="2" VarName="NDV_CHD" LoincScale="ORD">
        <FormItem ItemText="Koronar hjertesykdom" ItemHelp="Angina, hjerteinfarkt eller PCI/bypass" Visibility="1" Highlight="1" Optional="0" ClearStrategy="3">
          <Answer AnswerId="16884" OrderNumber="1" OptionText="Ja" VerboseText="Koronarsyk." ShortCode="J"/>
          <Answer AnswerId="16885" OrderNumber="2" OptionText="Nei" ShortCode="N"/>
          <Answer AnswerId="16886" OrderNumber="3" OptionText="Vet ikke" ShortCode="V"/>
        </FormItem>
      </Item>
    </Page>
    <Page PageTitle="Debut av koronarsykdom*">
      <Item ItemId="4055" ItemType="1" VarName="NDV_CHD_DEBUT" LoincScale="QN">
        <FormItem ItemText=" - første tilfelle, årstall" ItemHelp="Årstall for første tilfelle av enten angina/hjerteinfarkt/bypass /PCI" MinExpression="DOB_YYYY" MaxExpression="NOW_YYYY" Visibility="1" Highlight="1" Optional="1">
          <Answer/>
        </FormItem>
      </Item>
    </Page>
    <Page PageTitle="Atrieflimmer og hjerneslag*">
      <Item ItemId="3398" ItemType="2" VarName="NDV_STROKE" LoincScale="ORD">
        <FormItem ItemText="Hjerneslag" ItemHelp="Unntatt TIA" Visibility="1" Highlight="1" Optional="0" ClearStrategy="3">
          <Answer AnswerId="16887" OrderNumber="1" OptionText="Ja" VerboseText="Hjerneslag." ShortCode="J"/>
          <Answer AnswerId="16888" OrderNumber="2" OptionText="Nei" ShortCode="N"/>
          <Answer AnswerId="16889" OrderNumber="3" OptionText="Vet ikke" ShortCode="V"/>
        </FormItem>
      </Item>
    </Page>
    <Page PageTitle="Første hjerneslag*">
      <Item ItemId="4062" ItemType="1" VarName="NDV_STROKE_DEBUT" LoincScale="QN">
        <FormItem ItemText=" - første tilfelle, årstall" ItemHelp="Årstall for første tilfelle av hjerneslag" MinExpression="DOB_YYYY" MaxExpression="NOW_YYYY" Visibility="1" Highlight="1" Optional="1">
          <Answer/>
        </FormItem>
      </Item>
    </Page>
    <Page PageTitle="Retinopati*">
      <Item ItemId="4087" ItemType="2" VarName="NDV_RETINOPATHY" LoincScale="ORD">
        <FormItem ItemText="Diabetes retinopati" ItemHelp="Behandling inkluderer laserbehandling, injeksjonsbehandling og kirurgi." Visibility="1" Highlight="1" Optional="0" ClearStrategy="3">
          <Answer AnswerId="21836" OrderNumber="1" OptionText="Nei*" ShortCode="N"/>
          <Answer AnswerId="21837" OrderNumber="2" OptionText="Retinopati, ubehandlet." ICD10="H360"/>
          <Answer AnswerId="21838" OrderNumber="3" OptionText="Behandlet retinopati." ICD10="H360"/>
          <Answer AnswerId="21839" OrderNumber="4" OptionText="Ukjent*"/>
        </FormItem>
      </Item>
    </Page>
    <Page PageTitle="Første laserbehandling*">
      <Item ItemId="4205" ItemType="1" VarName="NDV_LASER_FIRST" LoincScale="QN">
        <FormItem ItemText=" - første laserbehandling, årstall" ItemHelp="Laserbehandling, injeksjon eller annen behandling for retinopati." MinExpression="DOB_YYYY" MaxExpression="NOW_YYYY" Visibility="1" Highlight="1" Optional="1">
          <Answer/>
        </FormItem>
      </Item>
    </Page>
    <Page PageTitle="Koronarsykdom i familien*">
      <Item ItemId="3342" ItemType="2" VarName="NDV_FAM_CHD" LoincScale="ORD">
        <FormItem ItemText="Tidlig koronarsykd. foreldre/søsken" ItemHelp="Før 55/65 år hos menn/kvinner, kun biologiske foreldre/søsken" Visibility="1" Highlight="1" Optional="1">
          <Answer AnswerId="16605" OrderNumber="1" OptionText="Ja" VerboseText="Tidlig koronarsykdom i familien." ShortCode="J"/>
          <Answer AnswerId="16606" OrderNumber="2" OptionText="Nei" ShortCode="N"/>
          <Answer AnswerId="16607" OrderNumber="3" OptionText="Vet ikke" ShortCode="V"/>
        </FormItem>
      </Item>
    </Page>
  </Form>
  <Actions>
    <PageAction MasterId="4207">
      <Activate EnumVal="2" PageConditionId="1651" PageNumber="4" ComparisonType="1" ComparisonDescription="Equal to"/>
    </PageAction>
    <PageAction MasterId="3397">
      <Activate EnumVal="1" PageConditionId="1652" PageNumber="8" ComparisonType="1" ComparisonDescription="Equal to"/>
    </PageAction>
    <PageAction MasterId="3398">
      <Activate EnumVal="1" PageConditionId="1641" PageNumber="10" ComparisonType="1" ComparisonDescription="Equal to"/>
    </PageAction>
    <PageAction MasterId="4087">
      <Activate EnumVal="3" PageConditionId="1642" PageNumber="12" ComparisonType="1" ComparisonDescription="Equal to"/>
    </PageAction>
  </Actions>
</StudyForm>
